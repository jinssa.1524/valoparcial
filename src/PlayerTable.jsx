import React, { useState } from 'react';
import './App.css'; // Importa tus estilos CSS aquí

const players = [
  { id: "01", name: "KEVIN HARRELL", team: "TOP CLUB", position: "GUARD" },
  { id: "02", name: "JAMES FLETCHER", team: "TOP CLUB", position: "CENTER" },
  { id: "03", name: "JEFF MONTES", team: "TOP CLUB", position: "FORWARD-GUARD" },
  { id: "04", name: "BRYAN WARNER", team: "TOP CLUB", position: "FORWARD-CENTER" },
  { id: "05", name: "YOSUF DALE", team: "TOP CLUB", position: "FORWARD" },
  { id: "06", name: "NOAH JONES", team: "TOP CLUB", position: "GUARD" },
  { id: "07", name: "BLESSED WHITE", team: "TOP CLUB", position: "CENTER" },
  { id: "08", name: "ALAN PARKET", team: "TOP CLUB", position: "FORWARD" },
  { id: "09", name: "NEYMAR TORRES", team: "TOP CLUB", position: "GUARD" },
  { id: "10", name: "JOSUE CASTRO", team: "TOP CLUB", position: "FORWARD-GUARD" },
  { id: "11", name: "JACK MOORE", team: "TOP CLUB", position: "FORWARD-CENTER" },
  { id: "12", name: "LIAM ADAMS", team: "TOP CLUB", position: "FORWARD" },
  { id: "13", name: "JUSTIN VASQUEZ", team: "TOP CLUB", position: "GUARD" },
  { id: "14", name: "ERICK CARRIS", team: "TOP CLUB", position: "CENTER" },
  { id: "15", name: "ELIAM MARQUEZ", team: "TOP CLUB", position: "FORWARD" },
  { id: "16", name: "DAVID LONDON", team: "TOP CLUB", position: "FORWARD-GUARD" },
  { id: "17", name: "JACOBO RODRIGUEZ", team: "TOP CLUB", position: "GUARD" },
  { id: "18", name: "JHEISON WALTER", team: "TOP CLUB", position: "CENTER" },
  { id: "19", name: "JUAN JOUNG", team: "TOP CLUB", position: "FORWARD-CENTER" },
  { id: "20", name: "MICHAEL THOMPSOM", team: "TOP CLUB", position: "FORWARD" },
];

const PlayerTable = () => {
  const [filterBy, setFilterBy] = useState('');
  const [searchTerm, setSearchTerm] = useState('');

  const applyFilter = (player) => {
    const playerNumber = parseInt(player.id);

    if (filterBy === 'paint-even' && playerNumber % 2 === 0) {
      return true;
    }
    if (filterBy === 'paint-odd' && playerNumber % 2 !== 0) {
      return true;
    }
    if (filterBy.includes('-')) {
      const [min, max] = filterBy.split('-').map(Number);
      if (playerNumber >= min && playerNumber <= max) {
        return true;
      }
      return false;
    }
    return true;
  };

  const handleSearch = (player) => {
    if (!searchTerm) return true;
    return player.name.toLowerCase().includes(searchTerm.toLowerCase());
  };

  const filteredPlayers = players.filter(player => applyFilter(player) && handleSearch(player));

  return (
    <section className="roster">
      <div className="container">
        <h1>ROSTER</h1>
        <div className="search-bar">
          <input
            className="search-name"
            type="text"
            placeholder="Buscar por nombre del jugador..."
            aria-placeholder="Buscar por nombre del jugador..."
            value={searchTerm}
            onChange={(e) => setSearchTerm(e.target.value)}
          />
        </div>
        <div className="filters container-fluid justify-content-start align-items-center">
          <button className="filter-btn" onClick={() => setFilterBy('paint-even')}>Pintar Pares</button>
          <button className="filter-btn" onClick={() => setFilterBy('paint-odd')}>Pintar Impares</button>
          <button className="filter-btn" onClick={() => setFilterBy('1-10')}>Traer de 1 a 10</button>
          <button className="filter-btn" onClick={() => setFilterBy('11-20')}>Traer de 11 a 20</button>
        </div>
        <table className="players-table">
          <thead>
            <tr>
              <th>#</th>
              <th>PLAYER</th>
              <th>TEAM</th>
              <th>POSITION</th>
            </tr>
          </thead>
          <tbody>
            {filteredPlayers.map((player, index) => (
              <tr key={player.id} className={index % 2 === 0 ? 'light-grey' : ''}>
                <td>{player.id}</td>
                <td>{player.name}</td>
                <td>{player.team}</td>
                <td>{player.position}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </section>
  );
};

export default PlayerTable;