import React from 'react';

const Footer = () => {
  return (
    <footer className="container-fluid">
      <div className="bi-media d-flex justify-content-center">
        <i className="bi bi-twitter"></i>
        <i className="bi bi-pinterest"></i>
        <i className="bi bi-telegram"></i>
        <i className="bi bi-reddit"></i>
        <i className="bi bi-envelope-at-fill"></i>
      </div>
    </footer>
  );
};

export default Footer;